#!/usr/bin/env python3

"""
** Contains the metadata. **
----------------------------

Allows you to avoid cyclical imports.
"""

__author__ = 'Robin RICHARD (robinechuca) <serveurpython.oz@gmail.com>'
__license__ = 'GNU Affero General Public License v3 or later (AGPLv3+)'
__version__ = '1.0.7'
