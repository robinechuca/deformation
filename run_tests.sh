#!/bin/bash


# install pyenv
pyenv --version > /dev/null
if [ $? -ne 0 ]; then
    read -p "'pyenv' is not install, do you want install it? (Y/n) " choice
    if [[ $choice == "y" || $choice == "Y" || -z $choice ]]; then
        echo "Well ok I will try to take care of it but I promise nothing!"
    else
        echo "You are free to install it manually!"
        exit 1
    fi

    # get dependencies
    which apt > /dev/null
    if [ $? -ne 0 ]; then
        which yum > /dev/null
        if [ $? -ne 0 ]; then
            echo "Sorry, I can't do it! https://devguide.python.org/setup/#build-dependencies"
            exit 1
        else
            sudo yum install zlib-devel bzip2 bzip2-devel readline-devel sqlite \
                sqlite-devel openssl-devel xz xz-devel libffi-devel gcc make
        fi
    else
    sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
        libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
        xz-utils tk-dev libffi-dev liblzma-dev python-openssl
    fi
    if [ $? -ne 0 ]; then
        echo "Sorry, I can't do it! https://devguide.python.org/setup/#build-dependencies"
        exit 1
    fi

    # install pyenv
    curl https://pyenv.run | bash
    if [ $? -ne 0 ]; then
        echo "Sorry, I can't do it!"
        exit 1
    fi
    echo ""
    echo "# >>> pyenv initialize >>>" >> ~/.bashrc
    echo "export PYENV_ROOT=\"$HOME/.pyenv\"" >> ~/.bashrc
    echo "export PIPENV_PYTHON=\"$PYENV_ROOT/shims/python\"" >> ~/.bashrc
    echo "export PATH=\"$HOME/.pyenv/bin:$PATH\"" >> ~/.bashrc
    echo "eval \"$(pyenv init -)\"" >> ~/.bashrc
    echo "eval \"$(pyenv virtualenv-init -)\"" >> ~/.bashrc
    echo "# <<< pyenv initialize <<<" >> ~/.bashrc

    exec $SHELL
fi

# install all python version, `pyenv install --list`
for version in 'system' '3.6.15' '3.7.13' '3.8.13' '3.9.13' '3.10.5'
do
    pyenv local $version
    if [ $? -ne 0 ]; then
        read -p "'python$version' is not install, do you want install it? (Y/n) " choice
        if [[ $choice == "y" || $choice == "Y" || -z $choice ]]; then
            cd ~/.pyenv/plugins/python-build/../.. && git pull && cd -
            pyenv install -v $version
        else
            echo "You are free to install it manually!"
            exit 1
        fi
        pyenv local $version
    fi
    for module in 'pip' 'numpy' 'opencv-python' 'sympy' 'torch' 'pytest' 'pylint' 'pdoc3' 'context-verbose' 'matplotlib' 'pyreverse'
    do
        python -m pip --timeout 600 install -U $module
    done
done

# test for each version
clear
for version in 'system' '3.6.15' '3.7.13' '3.8.13' '3.9.13' '3.10.5'
do
    pyenv local $version

    python -m pytest --full-trace --doctest-modules deformation/
    if [ $? -ne 0 ]; then
        pyenv local system
        exit 1
    fi
done
python -m pytest --full-trace deformation/test.py
if [ $? -ne 0 ]; then
    exit 1
fi

# test code quality
python -m pylint deformation/
if [ $? -ne 0 ]; then
    exit 1
fi

exit 0
